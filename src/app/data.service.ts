import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { IDriver, dLocation } from './interfaces';


@Injectable()
export class DataService {

  acak : number;
  peopleUrl : any;
  locationUrl : any;
  speciesUrl : any;
  starshipUrl : any;
  bioLuke : any;



  constructor(private http: HttpClient) {


    this.acak = Math.floor((Math.random() * 6) *1 )
    this.peopleUrl = 'https://swapi.co/api/people/' 
    // + this.acak;
    this.locationUrl = 'https://swapi.co/api/planets/?page='  + this.acak
    this.speciesUrl = 'https://swapi.co/api/species/'
    this.starshipUrl = 'https://swapi.co/api/starships/'
    this.bioLuke = 'https://swapi.co/api/people/1'

   }

  getDrivers() : Observable<IDriver[]> {
    return this.http.get<IDriver[]>(this.peopleUrl)
               .map(data => {
                 console.log(data);
                  return data['results'];
               });
  }

  getLocation() : Observable<dLocation[]> {
    return this.http.get<IDriver[]>(this.locationUrl)
               .map(data => {
                 console.log(data);
                  return data['results'];
               });
  }

  getBio (url:string) : Observable<any> {
    return this.http.get(url)
                .map(data => {
                  return [data];
                })
  }

  getStarship (url:string) : Observable<any> {
    return this.http.get(url)
                .map(data => {
                  return [data];
                })
    }

    

}
