import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';

import { HttpClientModule } from '@angular/common/http';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

import { AppComponent } from './app.component';
import { RoutingComponent } from "./app.routing";
import { MoviesIndexComponent } from './components/movies-index/movies-index.component';
import { MovieShowComponent } from './components/movie-show/movie-show.component';
import { MoviesThemoviedbService } from './services/movies-themoviedb.service';
import { MoviesSearchResultComponent } from './components/movies-search-result/movies-search-result.component'
import { PaginationBasicComponent } from './components/pagination/pagination-basic.component';

@NgModule({
  declarations: [
    AppComponent,
    MoviesIndexComponent,
    MovieShowComponent,
    MoviesSearchResultComponent,
    PaginationBasicComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RoutingComponent,
    JsonpModule,
    HttpClientModule,
    MDBBootstrapModule.forRoot(),
    InfiniteScrollModule,
  ],
  providers: [MoviesThemoviedbService],
  bootstrap: [AppComponent],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class AppModule { }
